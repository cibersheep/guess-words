#!/usr/bin/env python3

import json
import os
from os import path


base = path.abspath(path.join(
    path.dirname(path.realpath(__file__)),
    '../qml/categories'
))

print(base)

paths = []
for root, dirs, files in os.walk(base):
    for file in files:
        paths.append(path.join(root, file))

categories = {}
for file_path in paths:
    print(file_path)
    data = json.load(open(file_path, 'r', encoding='utf-8'))
    if data['category'] not in categories:
        categories[data['category']] = {}

    categories[data['category']][data['lang']] = {
        'title': data['title'],
        'description': data['description'],
    }

json.dump(categories, open(path.join(base, '../categories.json'), 'w'), indent=4)
