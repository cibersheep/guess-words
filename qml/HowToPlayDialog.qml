/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Component {
    id: howToPlayComponent

    Dialog {
        id: howToPlayDialog
        title: i18n.tr('How to Play')

        signal accepted()

        Label {
            anchors { left: parent.left; right: parent.right }
            wrapMode: Text.WordWrap
            maximumLineCount: Number.MAX_VALUE
            text: i18n.tr("Grab a partner, pick a category, and try to get them to guess the words on the screen before time runs out. Don't use rhyming words or spell out the word, but gestures are encouraged. Once your partner guesses the word hit the 'Next' button to get a new word. Feel free to tap the 'Pass' button if you or your partner is struggling (as an extra challenge, try playing without passing!). After time runs out, check your score and pass the phone to your friend to start the next round. Have fun guessing!")
        }

        Button {
            text: i18n.tr("Let's play!")
            color: theme.palette.normal.positive
            onClicked: {
                howToPlayDialog.accepted();
            }
        }
    }
}
