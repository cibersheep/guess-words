function load(url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', url);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            cb(JSON.parse(xhr.responseText));
        }
    };
    xhr.send();
}

function loadCategories(cb) {
    load('./categories.json', cb);
}

function loadCategory(category, cb) {
    if (category.indexOf('en_US') >= 0) {
        category = category.replace('en_US/', '');
    }

    load('./categories/' + category + '.json', cb);
}
