/*
 * Copyright (C) 2020  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * guess-words is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.settings 1.0

import "loader.js" as Loader

Page {
    id: categoryListPage

    Component.onCompleted: {
        if (settings.firstRun) {
            var popup = PopupUtils.open(howToPlayDialog)
            popup.accepted.connect(function() {
                PopupUtils.close(popup)
            });

            settings.firstRun = false;
        }

        Loader.loadCategories(function(data) {
            var categories = [];

            Object.keys(data).forEach(function(category) {
                var categoryData = data[category];

                var categoryTitle = categoryData['en_US'].title;
                if (categoryData[root.lang]) {
                    categoryTitle = categoryData[root.lang].title;
                }

                categories.push({
                    categoryTitle: categoryTitle,
                    categoryData: categoryData,
                    category: category,
                });
            });

            categories.sort(function(a, b) {
                if (a.categoryTitle > b.categoryTitle) {
                    return 1;
                }

                if (a.categoryTitle < b.categoryTitle) {
                    return -1;
                }

                return 0;
            });

            categories.forEach(function(categoryData) {
                categoryModel.append(categoryData);
            });
        });
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Guess Words')

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                }
            ]
        }

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }
    }

    Settings {
        id: settings

        property bool firstRun: true
    }

    ListModel {
        id: categoryModel
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        ListView {
            anchors.fill: parent
            model: categoryModel

            delegate: ListItem {
                height: layout.height

                ListItemLayout {
                    id: layout
                    title.text: categoryTitle

                    ProgressionSlot {}
                }

                onClicked: pageStack.push(Qt.resolvedUrl('CategoryLanguagePage.qml'), {
                    categoryTitle: categoryTitle,
                    categoryData: categoryData,
                    category: category,
                });
            }
        }
    }

    HowToPlayDialog {
        id: howToPlayDialog
    }
}
